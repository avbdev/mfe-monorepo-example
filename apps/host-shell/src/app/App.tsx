import * as React from 'react';

import NxWelcome from './nx-welcome';

import { Link, Route, Routes } from 'react-router-dom';

const RemoteAppTwo = React.lazy(() => import('remote-app-two/Module'));

const RemoteAppOne = React.lazy(() => import('remote-app-one/Module'));

export function App() {
  return (
    <React.Suspense fallback={null}>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/remote-app-two">RemoteAppTwo</Link>
        </li>
        <li>
          <Link to="/remote-app-one">RemoteAppOne</Link>
        </li>
      </ul>
      <Routes>
        <Route path="/" element={<NxWelcome title="host-shell" />} />
        <Route path="/remote-app-two" element={<RemoteAppTwo />} />
        <Route path="/remote-app-one" element={<RemoteAppOne />} />
      </Routes>
    </React.Suspense>
  );
}

export default App;
